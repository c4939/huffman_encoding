char_dict = {"a": "0", "b": "110", "c": "10", "d": "111"}
inv_map = {v: k for k, v in char_dict.items()}
char_dict = inv_map.copy()
char_vals = list(char_dict.keys())

binaries = "1100111"
width_step = 1


def slicer(bins):
    global width_step
    nelement = len(bins)
    slices = [bins[i:i + width_step] for i in range(0, nelement, width_step)]
    return slices


def encoder(bins):
    # slices = slicer(binaries)
    global width_step, char_vals
    index = 0
    chars = ""
    nelement = len(bins)
    seq = []
    while index < nelement:
        seq = bins[index:index+width_step]
        index += width_step
        while seq not in char_vals:
            seq = seq + bins[index]
            index += 1
        chars = chars + char_dict[seq]
    return chars




print(encoder(binaries))


